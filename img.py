from math import ceil
import json
import numpy
from PIL import Image
from icecream import ic

packets = json.load(open("packets_color.json"))
imgpacks = list(filter(lambda pack: int(pack["_source"]["layers"]["usb"]["usb.data_len"]) > 200, packets))
hexlists = [imgpack["_source"]["layers"]["usb.capdata"].split(":") for imgpack in imgpacks]
# usb return prefix
prefix_size = 10
data_hexlists = [hexlist[10:-1] for hexlist in hexlists]
data_hexlists = [[int(hexx, 16) for hexx in hexlist] for hexlist in data_hexlists]

def to_color(line):
    # apparently, the scanner gives all r lines then all g then all b
    bytes_per_color = int(len(line)/3)
    return list(zip(*[line[bytes_per_color*i:bytes_per_color*(i+1)] for i in range(3)]))

def trimstuff(trim_amount, line_per_read=16, color=True):
    bytes_per_line = int(len(data_hexlists[0])/line_per_read)
    all_lines = []
    for data in data_hexlists:
        for i in range(line_per_read):
            line = data[i*(bytes_per_line-trim_amount):(i+1)*(bytes_per_line-trim_amount)]
            if not color:
                all_lines.append(line)
            else:
                all_lines.append(to_color(line))

    img = Image.fromarray(numpy.uint8(all_lines))
    img.show()
    img.save(f"trimstuff{trim_amount}.bmp")


def tasoeur(line_per_read, trim_amount=0):
    lines = line_per_read * len(data_hexlists)

    all_the_pixels = numpy.asarray(data_hexlists).flatten()
    width = ic(len(all_the_pixels)/lines)
    ic(width)
    padding = round((ceil(width) - width) * lines)
    intwidth = ceil(width)

    ic(intwidth)
    ic(padding)
    ic(len(all_the_pixels))
    all_the_pixels = numpy.pad(all_the_pixels,(0, padding))
    ic(len(all_the_pixels))
    grid = all_the_pixels.reshape(lines,intwidth)
    grid = grid.reshape(lines,intwidth)
    img = Image.fromarray(numpy.uint8(grid))
    img.show()

trimstuff(3)
